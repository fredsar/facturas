package main

import (
	"context"
	"encoding/json"
	"io/ioutil"
	"log"
	"os"
	"os/signal"
	"time"

	firebase "firebase.google.com/go"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	"github.com/labstack/echo/middleware"
	"gitlab.com/fredsar/facturas/internal/consts"
	fErrors "gitlab.com/fredsar/facturas/internal/errors"
	"gitlab.com/fredsar/facturas/internal/services"
	"gitlab.com/fredsar/facturas/internal/store"
	"gitlab.com/fredsar/facturas/internal/transport"
	"gitlab.com/fredsar/facturas/internal/usecase"
	"google.golang.org/api/option"
)

type Config struct {
	Server     string `json:"server"`
	Port       string `json:"port"`
	Password   string `json:"password"`
	Username   string `json:"username"`
	Database   string `json:"database"`
	Production bool   `json:"production"`
}

func main() {
	var cfg Config
	bytes, err := ioutil.ReadFile("localconfig/config.json")
	if err != nil {
		log.Fatal("Cannot start without config file", err)
	}
	err = json.Unmarshal(bytes, &cfg)
	if err != nil {
		log.Fatal("Cannot start without config file", err)
	}

	if err := store.EnsurePostgresTables(
		cfg.Username, cfg.Password, cfg.Server,
		cfg.Port, cfg.Database, cfg.Production,
	); err != nil {
		log.Fatal("Error while initialing the tables", err)
	}

	store := store.NewStore(cfg.Username, cfg.Password, cfg.Server, cfg.Port, cfg.Database, cfg.Production)
	ctx := context.Background()
	opt := option.WithCredentialsFile(consts.FileFirebase)
	appFirebase, err := firebase.NewApp(ctx, nil, opt)
	if err != nil {
		log.Fatalln("Error incial al obtener el app", err)
	}

	firebaseClient, err := appFirebase.Auth(ctx)
	if err != nil {
		log.Fatalln("Error al obtener el cliente de firebase", err)
	}

	xlsxService := services.NewXLSXService()
	contadorUseCase := usecase.NewContador(store, xlsxService)

	contadorTransport := transport.NewContador(contadorUseCase, firebaseClient)
	router := transport.NewTransport(contadorTransport)

	router.Use(middleware.CORS())
	router.Use(middleware.Recover())
	router.Use(middleware.RemoveTrailingSlash())
	router.HTTPErrorHandler = fErrors.EchoErrorHandler()

	// Start server
	go func() {
		if err := router.Start(":8080"); err != nil {
			router.Logger.Info("shutting down the server")
		}
	}()

	// Handle graceful shutdown
	quit := make(chan os.Signal, 1)
	signal.Notify(quit, os.Interrupt)
	<-quit

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	if err := router.Shutdown(ctx); err != nil {
		router.Logger.Fatal(err)
	}
}
