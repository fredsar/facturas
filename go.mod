module gitlab.com/fredsar/facturas

go 1.13

require (
	firebase.google.com/go v3.13.0+incompatible
	github.com/dgrijalva/jwt-go v3.2.0+incompatible // indirect
	github.com/google/uuid v1.1.1
	github.com/jinzhu/gorm v1.9.12
	github.com/labstack/echo v3.3.10+incompatible
	github.com/labstack/gommon v0.3.0 // indirect
	github.com/tealeg/xlsx v1.0.5
	github.com/valyala/fasttemplate v1.1.0 // indirect
	google.golang.org/api v0.21.0
)
