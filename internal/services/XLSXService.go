package services

import (
	"strconv"

	"github.com/tealeg/xlsx"
	"gitlab.com/fredsar/facturas/internal/entity"
)

const (
	RFCCellValue = "RFC"
)

type XLSXService struct{}

func NewXLSXService() XLSXService {
	return XLSXService{}
}

func (s XLSXService) CreateXLSX(reporte *entity.Reporte) (*xlsx.File, error) {
	file := xlsx.NewFile()
	sheet, err := file.AddSheet("Hoja 1")
	if err != nil {
		return nil, err
	}
	fontBold := xlsx.NewFont(11, "Calibri")
	fontBold.Bold = true
	styleBold := xlsx.NewStyle()
	styleBold.Font = *fontBold

	fontPlain := xlsx.NewFont(11, "Calibri")
	fontBold.Bold = false
	stylePlain := xlsx.NewStyle()
	stylePlain.Font = *fontPlain

	InsertDatosCliente(reporte.RFCCliente, reporte.NombreCliente, styleBold, stylePlain, sheet)
	InsertHeader(styleBold, sheet)
	for _, resultado := range reporte.Resultados {
		InsertProvider(stylePlain, sheet, resultado)
	}

	return file, nil
}

func InsertDatosCliente(rfc, cliente string, styleBold, stylePlain *xlsx.Style, sheet *xlsx.Sheet) {
	row := sheet.AddRow()
	cell := row.AddCell()
	cell.SetStyle(styleBold)
	cell.Value = "Cliente"
	cell = row.AddCell()
	cell.SetStyle(stylePlain)
	cell.Value = cliente

	row = sheet.AddRow()
	cell = row.AddCell()
	cell.SetStyle(styleBold)
	cell.Value = RFCCellValue
	cell = row.AddCell()
	cell.SetStyle(stylePlain)
	cell.Value = rfc
}

func InsertHeader(styleBold *xlsx.Style, sheet *xlsx.Sheet) {
	row := sheet.AddRow()
	cell := row.AddCell()
	cell.SetStyle(styleBold)
	cell.Value = "Proveedor"

	cell = row.AddCell()
	cell.SetStyle(styleBold)
	cell.Value = RFCCellValue

	cell = row.AddCell()
	cell.SetStyle(styleBold)
	cell.Value = "Subtotal"

	cell = row.AddCell()
	cell.SetStyle(styleBold)
	cell.Value = "Descuentos"

	cell = row.AddCell()
	cell.SetStyle(styleBold)
	cell.Value = "Importe IVA"

	cell = row.AddCell()
	cell.SetStyle(styleBold)
	cell.Value = "Importe IEPS"

	cell = row.AddCell()
	cell.SetStyle(styleBold)
	cell.Value = "Total"

	cell = row.AddCell()
	cell.SetStyle(styleBold)
	cell.Value = "Desglose"

	cell = row.AddCell()
	cell.SetStyle(styleBold)
	cell.Value = "Base IVA 0"

	cell = row.AddCell()
	cell.SetStyle(styleBold)
	cell.Value = "Base IVA 16"

	cell = row.AddCell()
	cell.SetStyle(styleBold)
	cell.Value = "BASE IEPS"
}

func InsertProvider(stylePlain *xlsx.Style, sheet *xlsx.Sheet, resultados *entity.ResultadoProveedor) {
	row := sheet.AddRow()
	cell := row.AddCell()
	cell.SetStyle(stylePlain)
	cell.Value = resultados.Proveedor

	cell = row.AddCell()
	cell.SetStyle(stylePlain)
	cell.Value = resultados.RFCProveedor

	cell = row.AddCell()
	cell.SetStyle(stylePlain)
	cell.Value = strconv.FormatFloat(resultados.Subtotal, 'f', 2, 64)

	cell = row.AddCell()
	cell.SetStyle(stylePlain)
	cell.Value = strconv.FormatFloat(resultados.Descuento, 'f', 2, 64)

	cell = row.AddCell()
	cell.SetStyle(stylePlain)
	cell.Value = strconv.FormatFloat(resultados.ImporteIVA, 'f', 2, 64)

	cell = row.AddCell()
	cell.SetStyle(stylePlain)
	cell.Value = strconv.FormatFloat(resultados.ImporteIEPS, 'f', 2, 64)

	cell = row.AddCell()
	cell.SetStyle(stylePlain)
	cell.Value = strconv.FormatFloat(resultados.Total, 'f', 2, 64)

	cell = row.AddCell()
	cell.SetStyle(stylePlain)
	cell.Value = ""

	cell = row.AddCell()
	cell.SetStyle(stylePlain)
	cell.Value = strconv.FormatFloat(resultados.BaseIVA0, 'f', 2, 64)

	cell = row.AddCell()
	cell.SetStyle(stylePlain)
	cell.Value = strconv.FormatFloat(resultados.BaseIVA16, 'f', 2, 64)

	cell = row.AddCell()
	cell.SetStyle(stylePlain)
	cell.Value = strconv.FormatFloat(resultados.BaseIEPS, 'f', 2, 64)
}
