package transport

import (
	"firebase.google.com/go/auth"
	"github.com/labstack/echo"
	"gitlab.com/fredsar/facturas/internal/store"
)

type Handler struct {
	Store          *store.Store
	FirebaseClient *auth.Client
}

type (
	// ContadorTransport wraps endpoints for contador.
	ContadorTransport interface {
		GetContador(c echo.Context) error
		CreateContador(c echo.Context) error
		GetReporte(c echo.Context) error
		CreateReporte(c echo.Context) error
	}
)

func NewTransport(ct ContadorTransport) *echo.Echo {
	e := echo.New()

	contadores := e.Group("/contadores")
	contadores.GET("/:contador", ct.GetContador)
	contadores.POST("", ct.CreateContador)
	contadores.GET("/:contador/reportes/:reporte", ct.GetReporte)
	contadores.POST("/:contador/reportes", ct.CreateReporte)

	return e
}
