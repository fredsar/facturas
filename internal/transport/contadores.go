package transport

import (
	"context"
	"errors"
	"mime/multipart"
	"net/http"
	"strings"
	"time"

	"firebase.google.com/go/auth"
	"github.com/labstack/echo"
	"github.com/tealeg/xlsx"
	"gitlab.com/fredsar/facturas/internal/consts"
	"gitlab.com/fredsar/facturas/internal/entity"
	fErrors "gitlab.com/fredsar/facturas/internal/errors"
)

type (

	// ContadorUseCase wraps business logic for contador endpoints.
	ContadorUseCase interface {
		GetContadorByID(idContador string) (*entity.Contador, error)
		CreateContador(request entity.ContadorRequest) (*entity.Contador, error)
		CreateResumen(contadorID, clienteRFC, periodo string, archivos []*multipart.FileHeader) (*xlsx.File, error)
	}

	FirebaseClient interface {
		VerifyIDToken(ctx context.Context, tokenStr string) (*auth.Token, error)
		GetUser(ctx context.Context, id string) (*auth.UserRecord, error)
	}

	// Contador specific implementation for ContadorTransport.
	Contador struct {
		UseCase        ContadorUseCase
		FirebaseClient FirebaseClient
	}
)

func NewContador(cUC ContadorUseCase, fc FirebaseClient) Contador {
	return Contador{
		UseCase:        cUC,
		FirebaseClient: fc,
	}
}

func (t Contador) GetContador(c echo.Context) error {
	idContador := c.Param(consts.ParamContadorID)
	contador, err := t.UseCase.GetContadorByID(idContador)
	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, contador)
}

func (t Contador) CreateContador(c echo.Context) error {
	tokenStr := GetToken(c.Request())
	token, err := t.GetTokenFirebase(c.Request().Context(), tokenStr)
	if err != nil {
		return fErrors.Error{
			Cause:  err,
			Action: "error de servidor, por favor contacte al equipo correspondiente",
			Status: http.StatusInternalServerError,
			Code:   "e6549630-6f9c-4d7d-8e59-4a130f697c72",
		}
	}
	var contadorAux entity.ContadorRequest
	err = c.Bind(&contadorAux)
	if err != nil {
		return fErrors.Error{
			Cause:  err,
			Action: "the body was not properly sent",
			Status: http.StatusBadRequest,
			Code:   "be7b521a-f1a9-4302-8a39-120003aab9ed",
		}
	}

	if token.UID != contadorAux.ID {
		return fErrors.Error{
			Cause:  err,
			Action: "el contador es el único que puede modificar su propia información",
			Status: http.StatusBadRequest,
			Code:   "a70bba05-fecb-4d23-bd75-3b9a855a6acc",
		}
	}

	_, err = t.FirebaseClient.GetUser(c.Request().Context(), contadorAux.ID)
	if err != nil {
		return fErrors.Error{
			Cause:  err,
			Action: "error de servidor, por favor contacte al equipo correspondiente",
			Status: http.StatusInternalServerError,
			Code:   "41aec102-8bf5-41bd-803c-a8d1b72d50fc",
		}
	}

	cont, err := t.UseCase.CreateContador(contadorAux)
	if err != nil {
		return err
	}

	return c.JSON(http.StatusCreated, cont)
}

func (t Contador) GetReporte(c echo.Context) error {
	return nil
}

func (t Contador) CreateReporte(c echo.Context) error {
	usuario, err := t.GetIDUsuario(c.Request())
	if err != nil {
		return fErrors.Error{
			Cause:  err,
			Action: "el token de autenticación no es correcto",
			Status: http.StatusUnauthorized,
			Code:   "f7f9649b-84e5-4c1c-99a7-b91492400e99",
		}
	}
	rfc := c.Request().PostFormValue("rfc")
	if rfc == "" {
		return fErrors.Error{
			Action: "ingresa el RFC del cliente que desees asignar al reporte",
			Status: http.StatusBadRequest,
			Code:   "ad63da68-23a9-4ccf-9d89-e0a77aeadc4e",
		}
	}
	periodo := c.Request().PostFormValue("periodo")
	if periodo == "" {
		periodo = time.Now().UTC().Format("2006-01-02")
	}

	form, err := c.MultipartForm()
	if err != nil {
		return fErrors.Error{
			Cause:  err,
			Action: "los archivos no se pudieron leer, por favor envielos de manera correcta",
			Status: http.StatusBadRequest,
			Code:   "9dc93563-0a5f-488a-8827-ecfe8786aa37",
		}
	}

	files := form.File["archivos"]
	if len(files) == 0 {
		return fErrors.Error{
			Action: "archivos vacíos, debes enviar al menos uno",
			Status: http.StatusBadRequest,
			Code:   "066283a0-beeb-43e1-b97a-07e630fc81e0",
		}
	}

	reporte, err := t.UseCase.CreateResumen(usuario, rfc, periodo, files)
	if err != nil {
		return err
	}

	c.Response().Status = http.StatusOK
	c.Response().Header().Set("Content-Type", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
	err = reporte.Write(c.Response().Writer)
	if err != nil {
		return fErrors.Error{
			Cause:  err,
			Action: "error de servidor, por favor contacte al equipo correspondiente",
			Status: http.StatusInternalServerError,
			Code:   "9d718c19-e390-41cf-94ca-b005eaef720b",
		}
	}

	return nil
}

func GetToken(r *http.Request) string {
	token := r.Header.Get("Authorization")
	if token == "" {
		return ""
	}

	g := strings.Split(token, " ")
	if len(g) < 2 {
		return ""
	}

	return g[1]
}

func (t Contador) GetTokenFirebase(ctx context.Context, tokenStr string) (*auth.Token, error) {
	return t.FirebaseClient.VerifyIDToken(ctx, tokenStr)
}

func (t Contador) GetIDUsuario(r *http.Request) (usuario string, err error) {
	tokenStr := GetToken(r)
	if tokenStr == "" {
		return "", errors.New("la cabecera de autorización no es correcta")
	}

	token, err := t.GetTokenFirebase(r.Context(), tokenStr)
	if err != nil {
		return "", err
	}

	return token.UID, nil
}
