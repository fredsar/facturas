package entity

import "encoding/xml"

type Comprobante struct {
	XMLName           xml.Name    `xml:"Comprobante"`
	Total             float64     `xml:"Total,attr"`
	FormaPago         string      `xml:"FormaPago,attr"`
	SubTotal          float64     `xml:"SubTotal,attr"`
	Descuento         float64     `xml:"Descuento,attr"`
	Serie             string      `xml:"Serie,attr"`
	Folio             string      `xml:"Folio,attr"`
	Fecha             string      `xml:"Fecha,attr"`
	Moneda            string      `xml:"Moneda,attr"`
	MetodoPago        string      `xml:"MetodoPago,attr"`
	CondicionesDePago string      `xml:"CondicionesDePago,attr"`
	LugarExpedicion   string      `xml:"LugarExpedicion,attr"`
	NoCertificado     string      `xml:"NoCertificado,attr"`
	Emisor            Emisor      `xml:"Emisor"`
	Receptor          Receptor    `xml:"Receptor"`
	Sello             string      `xml:"Sello,attr"`
	Conceptos         []Concepto  `xml:"Conceptos>Concepto"`
	Impuestos         Impuestos   `xml:"Impuestos"`
	Complemento       Complemento `xml:"Complemento"`
}

type Complemento struct {
	XMLName             xml.Name            `xml:"Complemento"`
	TimbreFiscalDigital TimbreFiscalDigital `xml:"TimbreFiscalDigital"`
}

type TimbreFiscalDigital struct {
	UUID             string `xml:"UUID,attr"`
	RfcProvCertif    string `xml:"RfcProvCertif,attr"`
	NoCertificadoSAT string `xml:"NoCertificadoSAT,attr"`
	SelloSAT         string `xml:"SelloSAT,attr"`
	SelloCFD         string `xml:"SelloCFD,attr"`
	FechaTimbrado    string `xml:"FechaTimbrado,attr"`
}

type Emisor struct {
	XMLName       xml.Name `xml:"Emisor"`
	RFC           string   `xml:"Rfc,attr"`
	Nombre        string   `xml:"Nombre,attr"`
	RegimenFiscal string   `xml:"RegimenFiscal,attr"`
}

type Receptor struct {
	XMLName xml.Name `xml:"Receptor"`
	RFC     string   `xml:"Rfc,attr"`
	Nombre  string   `xml:"Nombre,attr"`
	UsoCFDI string   `xml:"UsoCFDI,attr"`
}

type Concepto struct {
	XMLName          xml.Name  `xml:"Concepto"`
	ClaveProdServ    int64     `xml:"ClaveProdServ,attr"`
	NoIdentificacion string    `xml:"NoIdentificacion,attr"`
	Cantidad         float64   `xml:"Cantidad,attr"`
	ClaveUnidad      string    `xml:"ClaveUnidad,attr"`
	Unidad           string    `xml:"Unidad,attr"`
	Descripcion      string    `xml:"Descripcion,attr"`
	ValorUnitario    float64   `xml:"ValorUnitario,attr"`
	Importe          float64   `xml:"Importe,attr"`
	Descuento        float64   `xml:"Descuento,attr"`
	Impuestos        Impuestos `xml:"Impuestos"`
}

type Impuestos struct {
	XMLName                   xml.Name    `xml:"Impuestos"`
	TotalImpuestosTrasladados float64     `xml:"TotalImpuestosTrasladados,attr"`
	TotalImpuestosRetenidos   float64     `xml:"TotalImpuestosRetenidos,attr"`
	Traslados                 []Traslado  `xml:"Traslados>Traslado"`
	Retenciones               []Retencion `xml:"Retenciones>Retencion"`
}

type Traslado struct {
	XMLName    xml.Name `xml:"Traslado"`
	Impuesto   string   `xml:"Impuesto,attr"`
	TipoFactor string   `xml:"TipoFactor,attr"`
	TasaOCuota float64  `xml:"TasaOCuota,attr"`
	Importe    float64  `xml:"Importe,attr"`
	Base       float64  `xml:"Base,attr"`
}

type Retencion struct {
	XMLName    xml.Name `xml:"Retencion"`
	Impuesto   string   `xml:"Impuesto,attr"`
	TipoFactor string   `xml:"TipoFactor,attr"`
	TasaOCuota float64  `xml:"TasaOCuota,attr"`
	Importe    float64  `xml:"Importe,attr"`
	Base       float64  `xml:"Base,attr"`
}
