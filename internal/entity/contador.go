package entity

import "time"

type Contador struct {
	ID                string     `json:"id" gorm:"PRIMARY_KEY"`
	RFC               string     `json:"rfc" gorm:"UNIQUE;type:varchar(13)"`
	Nombre            string     `json:"nombre"`
	PrimerApellido    string     `json:"primer_apellido"`
	SegundoApellido   string     `json:"segundo_apellido"`
	FechaAlta         time.Time  `json:"fecha_alta" gorm:"default:'now'"`
	ReportesRestantes int        `json:"reportes_restantes"`
	Reportes          []*Reporte `json:"reportes"`
}

type ContadorRequest struct {
	ID              string `json:"id"`
	RFC             string `json:"rfc"`
	Nombre          string `json:"nombre"`
	ApellidoPaterno string `json:"apellido_paterno"`
	ApellidoMaterno string `json:"apellido_materno"`
}
