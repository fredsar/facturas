package entity

import "time"

type Reporte struct {
	ID            string                `json:"id" gorm:"foreign_key;type:varchar(36)"`
	NombreCliente string                `json:"nombre_cliente"`
	RFCCliente    string                `json:"rfc_cliente"`
	Periodo       string                `json:"periodo"`
	Fecha         time.Time             `json:"fecha" gorm:"default:'now'"`
	ContadorID    string                `json:"contador_id"`
	Contador      *Contador             `json:"contador"`
	Resultados    []*ResultadoProveedor `json:"resultados"`
}

type ResultadoProveedor struct {
	RFCProveedor string  `json:"rfc_proveedor"`
	Proveedor    string  `json:"proveedor"`
	Subtotal     float64 `json:"subtotal"`
	Descuento    float64 `json:"descuento"`
	BaseIVA16    float64 `json:"base_iva_16"`
	BaseIVA0     float64 `json:"base_iva_0"`
	BaseIEPS     float64 `json:"base_ieps"`
	ImporteIVA   float64 `json:"importe_iva"`
	ImporteIEPS  float64 `json:"importe_ieps"`
	Total        float64 `json:"total"`
	ReporteID    string  `json:"reporte_id"`
}
