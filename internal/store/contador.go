package store

import (
	"fmt"
	"time"

	"github.com/jinzhu/gorm"
	"gitlab.com/fredsar/facturas/internal/entity"
)

func (s *Store) GetContadorByField(field, value string) (contador *entity.Contador, err error) {
	contador = &entity.Contador{}
	err = s.db.Preload("Reportes").
		Where(fmt.Sprintf("%s = ?", field), value).
		First(contador).Error
	if err != nil {
		if !gorm.IsRecordNotFoundError(err) {
			return nil, err
		}

		return nil, nil
	}

	return
}

func (s *Store) InsertContador(contador *entity.Contador) error {
	contador.FechaAlta = time.Now().UTC()
	tx := s.db.Begin()
	err := tx.Create(contador).Error
	if err != nil {
		tx.Rollback()

		return err
	}

	return tx.Commit().Error
}

func (s *Store) InsertResumen(resumen *entity.Reporte, contador *entity.Contador) error {
	tx := s.db.Begin()
	err := tx.Model(contador).Update(&entity.Contador{
		ReportesRestantes: contador.ReportesRestantes - 1,
	}).Error
	if err != nil {
		tx.Rollback()

		return err
	}
	err = tx.Create(resumen).Error
	if err != nil {
		tx.Rollback()

		return err
	}

	return tx.Commit().Error
}
