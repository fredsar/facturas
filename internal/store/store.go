package store

import (
	"fmt"
	"log"

	"github.com/jinzhu/gorm"
)

type (
	Store struct {
		db *gorm.DB
	}
)

func NewStore(username, password, server, port, database string, production bool) *Store {
	db, err := gorm.Open(
		"postgres",
		"postgres",
		fmt.Sprintf("host=%s port=%s user=%s dbname=%s password=%s sslmode=disable",
			server,
			port,
			username,
			database,
			password,
		),
	)
	if err != nil {
		log.Fatal("Error while trying to open the database connection", err)
	}
	db.DB().SetMaxOpenConns(500)
	db.DB().SetMaxIdleConns(500)
	db.SingularTable(true)
	if !production {
		db.LogMode(true)
	}

	return &Store{
		db: db,
	}
}

func (s Store) Close() {
	s.db.Close()
}
