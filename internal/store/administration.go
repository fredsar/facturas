package store

import (
	"log"

	"github.com/jinzhu/gorm"
	"gitlab.com/fredsar/facturas/internal/entity"
)

func EnsurePostgresTables(username, password, server, port, base string, production bool) error {
	st := NewStore(username, password, server, port, base, production)
	tx := st.db.Begin()

	if !tx.HasTable(&entity.Contador{}) {
		FatalDB(tx.AutoMigrate(&entity.Contador{}).Error, "No se pudo crear la tabla del contador", tx)
	}

	if !tx.HasTable(&entity.Reporte{}) {
		FatalDB(
			tx.AutoMigrate(&entity.Reporte{}).
				AddForeignKey(
					"contador_id",
					"contador(id)",
					"RESTRICT",
					"CASCADE",
				).Error,
			"No se pudo crear la tabla de reporte",
			tx,
		)
	}

	if !tx.HasTable(&entity.ResultadoProveedor{}) {
		FatalDB(
			tx.AutoMigrate(&entity.ResultadoProveedor{}).
				AddForeignKey(
					"reporte_id",
					"reporte(id)",
					"RESTRICT",
					"CASCADE",
				).Error,
			"No se pudo crear la tabla de ResultadoProveedor",
			tx,
		)
	}

	FatalDB(tx.Commit().Error, "Error al crear la tabla de contador", tx)

	return nil
}

func FatalDB(err error, mensaje string, tx *gorm.DB) {
	if err != nil {
		if tx != nil {
			tx.Rollback()
		}
		log.Fatal(mensaje + " " + err.Error())
	}
}
