package usecase

import "math"

func toTwoFixed(num float64) float64 {
	return toFixed(num, 2)
}

func toFixed(num float64, precision int) float64 {
	output := math.Pow(10, float64(precision))

	return float64(round(num*output)) / output
}

func round(num float64) int {
	return int(num + math.Copysign(0.5, num))
}
