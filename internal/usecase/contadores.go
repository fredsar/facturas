package usecase

import (
	"bytes"
	"encoding/xml"
	"io"
	"mime/multipart"
	"net/http"
	"time"

	"github.com/google/uuid"
	"github.com/tealeg/xlsx"
	"gitlab.com/fredsar/facturas/internal/entity"
	fErrors "gitlab.com/fredsar/facturas/internal/errors"
)

const (
	fieldID = "id"
)

type (
	ContadorStore interface {
		GetContadorByField(field, value string) (contador *entity.Contador, err error)
		InsertContador(contador *entity.Contador) error
		InsertResumen(resumen *entity.Reporte, contador *entity.Contador) error
	}

	XLSXService interface {
		CreateXLSX(reporte *entity.Reporte) (*xlsx.File, error)
	}

	Contador struct {
		Store       ContadorStore
		XLSXService XLSXService
	}
)

func NewContador(cS ContadorStore, xlS XLSXService) Contador {
	return Contador{
		Store:       cS,
		XLSXService: xlS,
	}
}

func (u Contador) GetContadorByID(idContador string) (*entity.Contador, error) {
	contador, err := u.Store.GetContadorByField(fieldID, idContador)
	if err != nil {
		return nil, fErrors.Error{
			Cause:  err,
			Action: "error de servidor, por favor contacte al equipo correspondiente",
			Status: http.StatusInternalServerError,
			Code:   "1e385f17-f366-4d82-b9f8-5bc35fe9f3a4",
		}
	}
	if contador == nil {
		return nil, fErrors.Error{
			Action: "el contador no fue encontrado, por favor revise sus datos",
			Status: http.StatusNotFound,
			Code:   "4c8938e8-7aad-461e-a94e-1be3f0d3de43",
		}
	}

	return contador, nil
}

func (u Contador) CreateContador(request entity.ContadorRequest) (*entity.Contador, error) {
	err := u.ValidaContador(request)
	if err != nil {
		return nil, err
	}
	cont := &entity.Contador{
		ID:              request.ID,
		RFC:             request.RFC,
		Nombre:          request.Nombre,
		PrimerApellido:  request.ApellidoPaterno,
		SegundoApellido: request.ApellidoMaterno,
	}

	err = u.Store.InsertContador(cont)
	if err != nil {
		return nil, fErrors.Error{
			Cause:  err,
			Action: "error de servidor al crear el contador, por favor contacte al equipo correspondiente",
			Status: http.StatusInternalServerError,
			Code:   "18f6dbef-599b-40a4-b55b-0516b8e873fd",
		}
	}

	return cont, nil
}

func (u Contador) ValidaContador(contadorAux entity.ContadorRequest) error {
	contadorAuxDB, err := u.Store.GetContadorByField(fieldID, contadorAux.ID)
	if err != nil {
		return fErrors.Error{
			Cause:  err,
			Action: "error de servidor, por favor contacte al equipo correspondiente",
			Status: http.StatusInternalServerError,
			Code:   "44f193ab-efce-4cfd-8bcf-acea76bd01a8",
		}
	}
	if contadorAuxDB != nil {
		return fErrors.Error{
			Action: "el contador ya existe, por favor revise sus datos",
			Status: http.StatusConflict,
			Code:   "a37e0f6e-94b3-4ed9-845e-c1e6da3c245d",
		}
	}

	contadorAuxDB, err = u.Store.GetContadorByField("rfc", contadorAux.RFC)
	if err != nil {
		return fErrors.Error{
			Cause:  err,
			Action: "error de servidor, por favor contacte al equipo correspondiente",
			Status: http.StatusInternalServerError,
			Code:   "b07b6e48-28fd-438a-93b9-ff91dde011c9",
		}
	}
	if contadorAuxDB != nil {
		return fErrors.Error{
			Action: "el contador ya existe, por favor revise sus datos",
			Status: http.StatusConflict,
			Code:   "4769f979-c4e0-45a0-a4a1-99731b85a713",
		}
	}

	return nil
}

func (u Contador) CreateResumen(
	contadorID, clienteRFC, periodo string, archivos []*multipart.FileHeader,
) (*xlsx.File, error) {

	contador, err := u.Store.GetContadorByField(fieldID, contadorID)
	if err != nil {
		return nil, fErrors.Error{
			Cause:  err,
			Action: "error de servidor, por favor contacte al equipo correspondiente",
			Status: http.StatusInternalServerError,
			Code:   "88cf7444-ada2-4613-b793-9460300271d4",
		}
	}
	if contador == nil {
		return nil, fErrors.Error{
			Action: "el contador no fue encontrado",
			Status: http.StatusNotFound,
			Code:   "fd7699b4-a9a8-4849-a2df-d56b0ade8578",
		}
	}
	comprobantes := make([]*entity.Comprobante, 0, len(archivos))
	for _, file := range archivos {
		data, err := file.Open()
		if err != nil {
			return nil, fErrors.Error{
				Cause:  err,
				Action: "error de servidor, por favor contacte al equipo correspondiente",
				Status: http.StatusInternalServerError,
				Code:   "09aab65a-7e96-4090-a6d8-3efab25e1e4a",
			}
		}

		out := bytes.NewBuffer(nil)
		_, err = io.Copy(out, data)
		if err != nil {
			return nil, fErrors.Error{
				Cause:  err,
				Action: "error de servidor, por favor contacte al equipo correspondiente",
				Status: http.StatusInternalServerError,
				Code:   "5f7e0caa-53c0-4566-8c05-9147d547a3a3",
			}
		}

		defer data.Close()
		dat := out.Bytes()
		comprobante, err := StringToComprobante(dat)
		if err != nil {
			return nil, fErrors.Error{
				Cause:  err,
				Action: "error de servidor, por favor contacte al equipo correspondiente",
				Status: http.StatusInternalServerError,
				Code:   "fee1701c-3b21-44f3-8c8c-31eb6997b270",
			}
		}
		comprobantes = append(comprobantes, comprobante)
	}
	resumen, err := CreateResumen(comprobantes, contadorID, clienteRFC, periodo)
	if err != nil {
		return nil, err
	}

	err = u.Store.InsertResumen(resumen, contador)
	if err != nil {
		return nil, fErrors.Error{
			Cause:  err,
			Action: "error de servidor, por favor contacte al equipo correspondiente",
			Status: http.StatusInternalServerError,
			Code:   "bb35c0bb-2a6a-4410-85c5-c7d1baa872dd",
		}
	}

	xlsx, err := u.XLSXService.CreateXLSX(resumen)
	if err != nil {
		return nil, fErrors.Error{
			Cause:  err,
			Action: "error de servidor, por favor contacte al equipo correspondiente",
			Status: http.StatusInternalServerError,
			Code:   "7e300e01-4438-491b-ada3-36be710e3e3e",
		}
	}

	return xlsx, nil
}

func StringToComprobante(dat []byte) (*entity.Comprobante, error) {
	var comprobante entity.Comprobante
	err := xml.Unmarshal(dat, &comprobante)
	if err != nil {
		return nil, err
	}

	return &comprobante, nil
}

func CreateResumen( // nolint: cyclop
	comprobantes []*entity.Comprobante, usuario, rfcCliente, periodo string,
) (*entity.Reporte, error) {

	res := make(map[string]*entity.ResultadoProveedor)
	for _, comprobante := range comprobantes {
		if rfcCliente != comprobante.Receptor.RFC {
			return nil, fErrors.Error{
				Action: "El RFC del receptor no corresponde en uno de los documentos",
				Status: http.StatusBadRequest,
				Code:   "b3503d81-3ef2-4d30-9b1f-38e4aac4b89f",
			}
		}
		var ieps, iva, total, subtotal, descuento, baseIVA0, baseIVA16, baseIEPS float64
		for _, concepto := range comprobante.Conceptos {
			for _, impuesto := range concepto.Impuestos.Traslados {
				if impuesto.Impuesto == "003" {
					ieps += toTwoFixed(impuesto.Importe)
					baseIEPS += toTwoFixed(impuesto.Base)
				}
				if impuesto.Impuesto == "002" {
					iva += toTwoFixed(impuesto.Importe)
					if impuesto.TasaOCuota == 0 {
						baseIVA0 += toTwoFixed(impuesto.Base)
					} else if impuesto.TasaOCuota == 0.16 {
						baseIVA16 += toTwoFixed(impuesto.Base)
					}
				}
			}
		}

		if res[comprobante.Emisor.RFC] == nil {
			total = comprobante.Total
			subtotal = comprobante.SubTotal
			descuento = comprobante.Descuento
		} else {
			total = comprobante.Total + res[comprobante.Emisor.RFC].Total
			subtotal = comprobante.SubTotal + res[comprobante.Emisor.RFC].Subtotal
			descuento = comprobante.Descuento + res[comprobante.Emisor.RFC].Descuento
			iva += res[comprobante.Emisor.RFC].ImporteIVA
			ieps += res[comprobante.Emisor.RFC].ImporteIEPS
			baseIEPS += res[comprobante.Emisor.RFC].BaseIEPS
			baseIVA0 += res[comprobante.Emisor.RFC].BaseIVA0
			baseIVA16 += res[comprobante.Emisor.RFC].BaseIVA16
		}
		res[comprobante.Emisor.RFC] = &entity.ResultadoProveedor{
			Proveedor:    comprobante.Emisor.Nombre,
			RFCProveedor: comprobante.Emisor.RFC,
			ImporteIVA:   toTwoFixed(iva),
			ImporteIEPS:  toTwoFixed(ieps),
			Descuento:    toTwoFixed(descuento),
			Subtotal:     toTwoFixed(subtotal),
			Total:        toTwoFixed(total),
			BaseIVA0:     baseIVA0,
			BaseIEPS:     baseIEPS,
			BaseIVA16:    baseIVA16,
		}
	}

	resultados := make([]*entity.ResultadoProveedor, 0)
	for _, valor := range res {
		resultados = append(resultados, valor)
	}

	return &entity.Reporte{
		ContadorID:    usuario,
		RFCCliente:    rfcCliente,
		NombreCliente: comprobantes[0].Receptor.Nombre,
		Resultados:    resultados,
		Fecha:         time.Now().UTC(),
		ID:            uuid.New().String(),
		Periodo:       periodo,
	}, nil
}
